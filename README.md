LazyClass
=========

```
pip install lazyclass
```


```python

from lazyclass import LazyClass

class MyClass(LazyClass):
    pass


c = MyClass()

@MyClass.method()
def new_method(self, a):
    print(a)


c.new_method()
```