
#!/usr/bin/env python

from distutils.core import setup

setup(
    name='lazyclass',
    version = '1.0.0',
    packages = ["lazyclass"],

    install_requires = [],

      
    # PyPI metadata
    description='Class for lazy adding of methods and properties',
    author='Callum Doolin',
    author_email='callum@resolvedinstruments.com',
    url = "https://gitlab.com/resolvedinstruments/lazyclass",
    license = "MIT",
    python_requires= '>=3.6',
    # project_urls={
    # },
    classifiers=[
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: 3.8',
    'Programming Language :: Python :: 3.9',
    ],

)
